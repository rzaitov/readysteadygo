/**
 *
 * @param {Sandbox} sb
 * @param {Date} date
 * @constructor
 */
function TimeRangeInDayItem(sb, date)
{
	/**
	 * @type {TimeRangeInDayItem}
	 */
	var that = this;

	/**
	 * @type {Sandbox}
	 * @private
	 */
	this._sb = sb;

	/**
	 * @type {Date}
	 * @private
	 */
	this._date = date;

	/**
	 * @type {?Date}
	 * @private
	 */
	this.start = null;

	/**
	 * @type {?Date}
	 * @private
	 */
	this.end = null;

	/**
	 * @type {?string}
	 * @private
	 */
	this._errorDescription = null;
}

/**
 * @return {?string}
 */
TimeRangeInDayItem.prototype.getErrorDescription = function()
{
	return this._errorDescription;
}

/**
 * @type {string}
 * @const
 */
TimeRangeInDayItem.prototype.noValue = "no_value";

/**
 * @return {string}
 * @public
 */
TimeRangeInDayItem.prototype.getStart = function()
{
	return this.getFormattedTimeProperty(this.start);
}

/**
 * @return {String}
 * @public
 */
TimeRangeInDayItem.prototype.getEnd = function()
{
	return this.getFormattedTimeProperty(this.end);
}

/**
 *
 * @param {?Date} date
 * @return {string}
 * @private
 */
TimeRangeInDayItem.prototype.getFormattedTimeProperty = function(date)
{
	/**
	 * @type {string}
	 */
	var result = this.noValue;

	if(date !== null)
	{
		result = this.convertDateToString(/**@type {Date}*/(date));
	}

	return result;
}

/**
 *
 *
 * @param {Date} date
 * @return {string}
 * @private
 */
TimeRangeInDayItem.prototype.convertDateToString = function(date)
{
	/**
	 * @type {Number}
	 */
	var hh = date.getHours();

	/**
	 * @type {Number}
	 */
	var mm = date.getMinutes();

	// return value in hh:mm format
	return hh + ':' + mm;
}

/**
 * @param {TimeRangeCollection} parent
 * @constructor
 */
TimeRangeInDayItem.prototype.SetParent = function(parent)
{
	/**
	 * @type {TimeRangeCollection}
	 * @public
	 */
	this.parent = parent;
}

/**
 *
 * @param {string} startTime
 * @param {string} endTime
 */
TimeRangeInDayItem.prototype.setTimeRange = function(startTime, endTime)
{
	this.start = this.convertToDate(startTime);
	this.end = this.convertToDate(endTime);

	this.checkRules();

	var eventArgs = { sender : this };
	this._sb.notifyObservers('timeRangeChanged', eventArgs);
}

/**
 * @param {?string} time
 * @return {?Date}
 * @private
 */
TimeRangeInDayItem.prototype.convertToDate = function(time)
{
	/**
	 * @type {?Date}
	 */
	var result = null;

	if(time !== this.noValue)
	{
		/**
		 * @type {Array.<Number>}
		 */
		var hh_mm = this.parseHoursAndMinutes(time);

		/**
		 * @type {Date}
		 */
		result = new Date(this._date.getTime());

		result.setHours(hh_mm[0]);
		result.setMinutes(hh_mm[1]);
	}

	return result;
}

/**
 *
 * @param {string} time — время в формате hh:mm
 * @return {Array.<Number>}
 * @private
 */
TimeRangeInDayItem.prototype.parseHoursAndMinutes = function(time)
{
	/**
	 * @type {RegExp}
	 */
	var timePattern = /^[0-9]{2}:[0-9]{2}$/;

	if(time.match(timePattern) == null)
	{
		throw {msg : "IncorrectTimeFormat"};
	}

	/**
	 * @type {Number}
	 */
	var hh = parseInt(/^[0-9]{2}/.exec(time));

	/**
	 * @type {string}
	 */
	var minutesString = /[0-9]{2}$/.exec(time);
	/**
	 * @type {Number}
	 */
	var mm = parseInt(minutesString);

	/**
	 * @type {Array.<Number>}
	 */
	return [hh, mm];
}


/**
 * @private
 */
TimeRangeInDayItem.prototype.checkRules = function()
{
	this._errorDescription = null;
	var error = {};

	if(!this.boundariesAreSet(error))
	{
		this._errorDescription = null;
		this._errorDescription = error.Description;
	}
	else if (!this.rightOrder(error))
	{
		this._errorDescription = null;
		this._errorDescription = error.Description;
	}
}

// #region ValidationRules
/**
 * Либо обе границы заданы либо обе не заданы
 * @param {Object} arg
 * @return {boolean}
 * @private
 */
TimeRangeInDayItem.prototype.boundariesAreSet = function(arg)
{
	arg.Description = null;
	var result = true;

	var bothAreSet = this.start !== null && this.end !== null;
	var bothAreNotSet = this.start === null && this.end === null;

	if(!(bothAreSet || bothAreNotSet))
	{
		arg.Description = "Необходимо задать начало и конец промежутка";
		result = false;
	}

	return result;
}

/**
 * Конец должен идти позже чем начало
 */
TimeRangeInDayItem.prototype.rightOrder = function(arg)
{
	arg.Description = null;
	var result = true;

	var bothAreSet = this.start !== null && this.end !== null;

	if(bothAreSet)
	{
		if(this.start.getTime() >= this.end.getTime())
		{
			arg.Description = "Конец должен быть позже чем начало";
			result = false;
		}
	}

	return result;
}
// #endregion