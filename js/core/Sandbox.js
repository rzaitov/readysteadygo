/**
 * Песочница
 * @constructor
 */
function Sandbox()
{
	/**
	 *
	 * @type {Array.<ObserverWrapper>}
	 * @private
	 */
	this.observers = [];
}

/**
 * @param {function(string, Object)} observer
 * @param {?Object=} context
 */
Sandbox.prototype.addObserver = function(observer, context)
{
	if(typeof observer !== 'function')
	{
		throw new Error('observer must be a function');
	}

	context = (context === undefined) ? null : context;
	var observerWrapper = new ObserverWrapper(observer, context);

	for(var i = 0, len = this.observers.length; i < len; i += 1)
	{
		/**
		 * @type {ObserverWrapper}
		 */
		var currObsWrp = this.observers[i];
		if(currObsWrp.observer === observer && currObsWrp.context === context)
		{
			throw new Error('observer already in the collection');
		}
	}

	this.observers.push(observerWrapper);
};

/**
 *
 * @param {function(string, Object)} observer
 * @param {?Object} context
 * @return {boolean}
 */
Sandbox.prototype.removeObserver = function(observer, context)
{
	for(var i = 0, len = this.observers.length; i < len; i += 1)
	{
		/**
		 * @type {ObserverWrapper}
		 */
		var currObsWrp = this.observers[i];
		if(currObsWrp.observer === observer && currObsWrp.context === context)
		{
			this.observers.splice(i, 1);
			return true;
		}
	}

	throw new Error('удаляемый observer не найден в коллекции');
};

/**
 *
 * @param {string} eventName
 * @param {Object} data
 */
Sandbox.prototype.notifyObservers = function(eventName, data)
{
	var observersSnapshot = this.observers.slice(0);
	for(var i = 0, len = observersSnapshot.length; i < len; i += 1)
	{
		/**
		 * @type {ObserverWrapper}
		 */
		var currObsWrp = this.observers[i];
		currObsWrp.observer.apply(currObsWrp.context, [eventName, data]);
	}
};

/**
 * @param {*} value
 * @public
 */
Sandbox.prototype.log = function(value)
{
	console.log(value);
};

/**
 * @param {function(string, Object)} observer
 * @param {?Object} context
 * @constructor
 */
function ObserverWrapper(observer, context)
{
	/**
	 * @type {function(string, Object)}
	 */
	this.observer = observer;

	/**
	 * @type {?Object}
	 */
	this.context = context;
}


/**
 * Глобальная песочница
 * @type {Sandbox}
 */
Rsg.sandbox = new Sandbox();
