/**
 * Расширяет один объект свойствами другого. Если свойства уже есть в целевом объекте они будут затерты
 * @param sourceObject {Object} Объект, свойства которого будут скопированы
 * @param targetObject {Object} Объект, который необходимо расширить
 * @return {Object} Расширенный объект
 */
function extend(sourceObject, targetObject)
{
    for(var prop in sourceObject)
    {
        targetObject[prop] = sourceObject[prop];
    }

    return targetObject;
}

/**
 * Расширяет объект полями (свойствами, которые не являются функциями) другого объекта
 * @param sourceObject {Object} Объект, поля которого будут скопированы
 * @param targetObject {Object} Объект, который необходимо расширить
 * @return {Object} Расширенный объект
 */
function extendProperties(sourceObject, targetObject)
{
    for(var prop in sourceObject)
    {
        if(typeof sourceObject[prop] !== "function")
        {
            targetObject[prop] = sourceObject[prop];
        }

    }

    return targetObject;
}