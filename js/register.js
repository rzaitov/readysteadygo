$(document).ready(function()
{
	/**
	 * @type {Sandbox}
	 */
	var sb = Rsg.sandbox;

	/**
	 *
	 * @type {jQueryObject}
	 */
	var jInputName = $('.name_block');
	sb.log(jInputName[0]);
	var inputName = new InputNameHtmlField(jInputName);

	/*
	var jFreeTime = $('.free_time_block');;
	sb.log(jFreeTime[0]);
	var freeTime = new FreeTimeHtmlField('freeTime', jFreeTime);
*/
	var jPhone = $('.phone_block');
	sb.log(jPhone[0]);
	var phoneField = new PhoneHtmlField(jPhone);

	var jFirstWeekFreeTime = $('.free_time_on_week');
	var jFreeTimeError = $('.free_time_block_error');
	var freeTimeOnFirstWeek = new FreeTimeOnWeek(Rsg.sandbox, jFirstWeekFreeTime, jFreeTimeError);

	sb.addObserver(
		/**
		 *
		 * @param {string} eventName
		 * @param {Object} data
		 */
		function(eventName, data)
		{
			sb.log('new value: ' + /**@type {string}*/ (data.newValue));
		});

	sb.addObserver(
		/**
		 *
		 * @param {string} eventName
		 * @param {Object} data
		 */
		function(eventName, data)
		{
			sb.log('free time: ' + /**@type {string}*/ (data.newValue));
		});

	sb.addObserver(
		/**
		 *
		 * @param {string} eventName
		 * @param {Object} data
		 */
			function(eventName, data)
		{
			sb.log('phone: ' + /**@type {string}*/ (data.newValue));
		});

});