/**
 * Поле ввода данных
 * @constructor
 * @param {?string} inputName
 * @param {Sandbox} sb
 */
function InputFieldAbstract(inputName, sb)
{
    /**
     * Текущее значение поля
     * @type {string}
     * @protected
     */
    this._value = '';

    /**
     * Визуальное состояние формы ввода
     * @type {VisualState}
     * @protected
     */
    this._visualState = VisualState.Normal;

	/**
	 * Имя поля, например: "FirstName" или "LastName" или "FullName"
	 * @type {?string}
	 */
	this.inputName = inputName;

	/**
	 * Флаг — обязательное поле или нет
	 * @type {boolean}
	 */
	this.isRequired = false;

	/**
	 *
	 * @type {Sandbox}
	 * @protected
	 */
	this.sandbox = sb;
}
/**
 * Указывает валидно ли введенное значение
 * @public
 * @return {boolean}
 */
InputFieldAbstract.prototype.isValueValid = function() { throw { message : "NotImplementException" }; }

/**
 * Переводит элемент управления в нормальное состояние
 * @public
 */
InputFieldAbstract.prototype.setNormalState = function()
{
	Rsg.sandbox.log('InputFieldAbstract.prototype.setNormalState');

	this._visualState = VisualState.Normal;
};

/**
 * Переводит элемент управления в валидное(проверенное) состояние
 * @public
 */
InputFieldAbstract.prototype.setValidState = function()
{
    this._visualState = VisualState.Valid;
}

/**
 * Переводит элемент управления в не валидное состояние
 * @public
 */
InputFieldAbstract.prototype.setInvalidState = function()
{
    this._visualState = VisualState.Invalid;
}

/**
 * @public
 */
InputFieldAbstract.prototype.updateVisualState = function()
{
	if(this.isValueValid())
	{
		this.setValidState();
	}
	else
	{
		this.setInvalidState();
	}
}

/**
 * @public
 */
InputFieldAbstract.prototype.showErrorMessage = function()
{
	throw {message: "NotImplementedException"};
}

/**
 * @public
 */
InputFieldAbstract.prototype.hideErrorMessage = function()
{
	throw {message: "NotImplementedException"};
}

/**
 * Уведомить слушаетелей об изменении значения
 * @protected
 */
InputFieldAbstract.prototype.RaiseValueChanged = function()
{
	var eventArg = {
		newValue : this._value
	};

	this.sandbox.notifyObservers(this.inputName + 'Changed', eventArg)
}