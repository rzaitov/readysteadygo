/**
 *
 * @constructor
 * @extends PhoneField
 */
function PhoneHtmlField(jRoot)
{
	/**
	 * @type {PhoneHtmlField}
	 */
	var that = this;

	/**
	 * @type {jQueryObject}
	 * @private
	 */
	this.jRoot = jRoot;

	/**
	 * @type {jQueryObject}
	 * @private
	 */
	this.jInput = this.jRoot.find('.inputField');

	this.jInput.focus(function()
	{
		that.setNormalState();
	});

	this.jInput.blur(function()
	{
		/**
		 * @type {string}
		 */
		var rawPhone = /**@type {string}*/ (that.jInput.val());
		that.sandbox.log('rawPhone: ' + rawPhone);

		that._value = that.getPhoneNumber(rawPhone);
		that.sandbox.log('phone value: ' + that._value);
		that.RaiseValueChanged();

		that.updateVisualState();
	});

	/**
	 * @type {jQueryObject}
	 * @private
	 */
	this.jError = this.jRoot.find('.error_message');

	/**
	 * @type {jQueryObject}
	 * @private
	 */
	this.jDescription = this.jRoot.find('.description');
}

PhoneHtmlField.prototype = new PhoneField();

/**
 * @public
 * @override
 */
PhoneHtmlField.prototype.setNormalState = function()
{
	this.jRoot.removeClass('success').removeClass('error');
	this.hideErrorMessage();

	var base = Object.getPrototypeOf(PhoneHtmlField.prototype);
	base.setNormalState.apply(base, null);
};

/**
 * @public
 * @override
 */
PhoneHtmlField.prototype.setValidState = function()
{
	this.jRoot.addClass('success');
	this.hideErrorMessage();

	var base = Object.getPrototypeOf(PhoneHtmlField.prototype);
	base.setValidState.apply(base, null);
};

/**
 * @public
 * @override
 */
PhoneHtmlField.prototype.setInvalidState = function()
{
	this.jRoot.addClass('error');
	this.showErrorMessage();

	var base = Object.getPrototypeOf(PhoneHtmlField.prototype);
	base.setInvalidState.apply(base, null);
};

/**
 * @public
 * @override
 */
PhoneHtmlField.prototype.showErrorMessage = function()
{
	this.jError.show();
	this.jDescription.hide();
};

/**
 * @public
 * @override
 */
PhoneHtmlField.prototype.hideErrorMessage = function()
{
	this.jError.hide();
	this.jDescription.show();
};