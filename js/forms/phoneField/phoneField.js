/**
 *
 * @constructor
 * @extends InputFieldAbstract
 */
function PhoneField()
{

}


PhoneField.prototype = new InputFieldAbstract('phone', Rsg.sandbox);

/**
 *
 * @param {string} phoneString
 * @return {string}
 */
PhoneField.prototype.getPhoneNumber = function(phoneString)
{
	// Удаляем все пробелы
	var phone = phoneString;
	this.sandbox.log(phone + '');

	phone = phone.replace(/\s+/g, '');
	this.sandbox.log(phone + '');

	// Удаляем тире и дефисы
	phone = phone.replace(/[-—]/g, '');
	this.sandbox.log(phone + '');

	// Убираем скобки если были
	phone = phone.replace(/\(/g, '').replace(/\)/g,'');
	this.sandbox.log(phone + '');

	// заменяем +7 в начале строки на 8
	phone = phone.replace(/^\+7/, '8');
	this.sandbox.log(phone + '');

	return phone;
};

/**
 * @public
 * @override
 */
PhoneField.prototype.isValueValid = function()
{
	/**
	 * @type {string}
	 */
	var phone = this.getPhoneNumber(this._value);

	var digitCnt = phone.length;

	// 7 цифр для городских (федеральных) номеров
	// 11 для мобильников
	return (digitCnt === 7 || digitCnt === 11);
}