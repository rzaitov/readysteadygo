/**
 *
 * @constructor
 * @extends FreeTextField
 * @param {!string} inputName
 * @param {jQueryObject} jRoot
 */
function FreeTimeHtmlField(inputName, jRoot)
{
	/**
	 *
	 * @type {FreeTimeHtmlField}
	 */
	var that = this;

	this.inputName = inputName;

	this.jRoot = jRoot;

	this.jInput = this.jRoot.find('.inputField');
	this.jError = this.jRoot.find('.help-inline');

	this.jInput.focusin(function()
	{
		that.setNormalState();
	});

	this.jInput.blur(function()
	{
		that._value = /**@type string*/(that.jInput.val()).trim();
		that.RaiseValueChanged();

		that.updateVisualState();
	});
}

FreeTimeHtmlField.prototype =  new FreeTextField();

/**
 * @public
 * @override
 */
FreeTimeHtmlField.prototype.setNormalState = function()
{
	this.jRoot.removeClass('success').removeClass('error');
	this.hideErrorMessage();

	var base = Object.getPrototypeOf(FreeTimeHtmlField.prototype);
	base.setNormalState.apply(base, null);
};
/**
 * @public
 * @override
 */
FreeTimeHtmlField.prototype.setValidState = function()
{
	this.jRoot.addClass('success');
	this.hideErrorMessage();

	var base = Object.getPrototypeOf(FreeTimeHtmlField.prototype);
	base.setValidState.apply(base, null);
};
/**
 * @public
 * @override
 */
FreeTimeHtmlField.prototype.setInvalidState = function()
{
	this.jRoot.addClass('error');
	this.showErrorMessage();

	var base = Object.getPrototypeOf(FreeTimeHtmlField.prototype);
	base.setInvalidState.apply(base, null);

};

/**
 * @public
 * @override
 */
FreeTimeHtmlField.prototype.showErrorMessage = function()
{
	this.jError.show();
};

/**
 * @public
 * @override
 */
FreeTimeHtmlField.prototype.hideErrorMessage = function()
{
	this.jError.hide();
};

