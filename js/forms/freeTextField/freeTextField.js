/**
 *
 * @constructor
 * @extends InputFieldAbstract
 */
function FreeTextField()
{

}

FreeTextField.prototype = new InputFieldAbstract(null, Rsg.sandbox);

FreeTextField.prototype.isValueValid = function()
{
	var trimmedText = this._value.trim();
	return trimmedText !== '';
}