/**
 * Визуальные состояния элементов ввода
 * @enum {number}
 */
var VisualState = {
    Normal : 0,
    Valid : 1,
    Invalid : 2
};
