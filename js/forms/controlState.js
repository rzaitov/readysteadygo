/**
 * @enum {number}
 */
var ControlState = {
	Enabled : 1,
	Disabled : 2
}
