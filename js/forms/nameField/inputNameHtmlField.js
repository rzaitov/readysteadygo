/**
 *
 * @param {jQueryObject} jRoot
 * @constructor
 */
function InputNameHtmlField(jRoot)
{
	/**
	 * @type {InputNameHtmlField}
	 */
	var that = this;
	/**
	 * Корневой элемент блока ввода
	 * @type {jQueryObject}
	 * @private
	 */
	this._html = jRoot;

	/**
	 *
	 * @type {jQueryObject}
	 * @private
	 */
	this.jInput = jRoot.find('.inputField');

	/**
	 *
	 * @type {jQueryObject}
	 * @private
	 */
	this.jError = jRoot.find('.help-inline');

	this.jInput.focusin(function()
	{
		that.setNormalState();
	});

	this.jInput.blur(function()
	{
		that._value = /** @type {string} */ (that.jInput.val());
		that.RaiseValueChanged();

		that.updateVisualState();
	});
}

InputNameHtmlField.prototype = new InputNameField();

/**
 * @public
 * @override
 */
InputNameHtmlField.prototype.setNormalState = function()
{
	Rsg.sandbox.log('InputNameHtmlField.prototype.setNormalState');
	this._html.removeClass('success').removeClass('error');
	this.hideErrorMessage();

	var base = Object.getPrototypeOf(InputNameHtmlField.prototype);
	base.setNormalState.apply(base, null);
}

/**
 * @public
 * @override
 */
InputNameHtmlField.prototype.setValidState = function()
{
	this._html.addClass('success');
	this.hideErrorMessage();

	var base = Object.getPrototypeOf(InputNameHtmlField.prototype);
	base.setValidState.apply(base, null);
}

/**
 * @public
 * @override
 */
InputNameHtmlField.prototype.setInvalidState = function()
{
	this._html.addClass('error');
	this.showErrorMessage();

	var base = Object.getPrototypeOf(InputNameHtmlField.prototype);
	base.setInvalidState.apply(base, null);
}

/**
 * @public
 * @override
 */
InputNameHtmlField.prototype.showErrorMessage = function()
{
	this.jError.show();
}

/**
 * @public
 * @override
 */
InputNameHtmlField.prototype.hideErrorMessage = function()
{
	this.jError.hide();
}

