/**
 * Поле для ввода имени, фамилии или отчества
 * @constructor
 * @extends InputFieldAbstract
 */
function InputNameField()
{
}

InputNameField.prototype = new InputFieldAbstract('Name', Rsg.sandbox);

/**
 * @override
 */
InputNameField.prototype.isValueValid = function ()
{
	var namePattern = /^[\u0400-\u04FF]+$/; // Символы кириллицы
	return this._value.match(namePattern) != null;
};


