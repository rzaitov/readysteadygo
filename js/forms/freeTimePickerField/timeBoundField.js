/**
 * @param {Sandbox} sb
 * @param {jQueryObject} jRoot
 * @param {BoundType} boundType
 * @constructor
 */
function TimeBoundField(sb, jRoot, boundType)
{
	/**
	 * @type {TimeBoundField}
	 */
	var that = this;

	/**
	 * @type {Sandbox}
	 * @private
	 */
	this._sb = sb;

	/**
	 * @type {jQueryObject}
	 * @private
	 */
	this._jRoot = jRoot;

	/**
	 * @type {BoundType}
	 * @private
	 */
	this._boundType = boundType;

	/**
	 * @type {jQueryObject}
	 * @private
	 */
	this._jSelect = this._jRoot.find('.select_time');

	/**
	 * @type {jQueryObject}
	 * @private
	 */
	this._jNoValueOption = this._jSelect.find('[' + this.noValue +']');
	/**
	 * @type {jQueryObject}
	 * @private
	 */
	this._jDefaultValue = this._jSelect.find('[' + this.defaultValue +']');

	this._jSelect.change(function()
	{
		that.raiseValueChangedEvent();
	});
}

/**
 * @type {string}
 * @const
 * @public
 */
TimeBoundField.prototype.noValue = 'no_value';

/**
 * @type {string}
 * @const
 * @public
 */
TimeBoundField.prototype.defaultValue = 'default_value';

/**
 * @protected
 */
TimeBoundField.prototype.raiseValueChangedEvent = function()
{
	var newValue = this.getSelectedValue();
	this._sb.notifyObservers('timeBoundFieldChanged', {sender : this, newValue : newValue});
}

/**
 * @param {ControlState} state
 */
TimeBoundField.prototype.setState = function(state)
{
	switch (state)
	{
		case ControlState.Disabled :
			this.setDisabledState();
			break;

		case ControlState.Enabled :
			this.setEnabledState();
			break;

		default :
			throw {msg : "NotImplementedException"};
	}
}

TimeBoundField.prototype.setDisabledState = function()
{
	this._jSelect.attr('disabled', 'disabled');

}

TimeBoundField.prototype.setEnabledState = function()
{
	this._jSelect.removeAttr('disabled');
}

TimeBoundField.prototype.setDefaultValue = function()
{
	this._jDefaultValue.prop('selected', true);
	this.raiseValueChangedEvent();
}

TimeBoundField.prototype.setNoValue = function()
{
	this._jNoValueOption.prop('selected', true);
	this.raiseValueChangedEvent();
}

/**
 * @return {boolean}
 * @public
 */
TimeBoundField.prototype.isValueSet = function()
{
	return this.getSelectedValue() !== this.noValue;
}

/**
 * @return {string}
 * @private
 */
TimeBoundField.prototype.getSelectedValue = function()
{
	return /**@type {string}*/(this._jSelect.val());
}

