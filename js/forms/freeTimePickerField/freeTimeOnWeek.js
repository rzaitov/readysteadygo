/**
 * @param {Sandbox} sb
 * @param {jQueryObject} jRoot
 * * @param {jQueryObject} jError
 * @constructor
 */
function FreeTimeOnWeek (sb, jRoot, jError)
{
	/**
	 * @type {FreeTimeOnWeek}
	 */
	var that = this;

	/**
	 * @type {Sandbox}
	 * @private
	 */
	this._sb = sb;

	/**
	 * @type {jQueryObject}
	 * @private
	 */
	this._jRoot = jRoot;

	/**
	 * @type {jQueryObject}
	 * @private
	 */
	this._jError = jError;

	/**
	 * @type {string}
	 */
	var currentDateStr = /**@type {string}*/ (this._jRoot.attr('current_date'));
	this.currentDate = new Date(currentDateStr);
	this._sb.log(currentDateStr);

	/**
	 * @type {Array.<FreeTimeItem>}
	 * @private
	 */
	this._freeTimeItems = [];

	this._jRoot.focusout(function()
	{
		if(that.isValid())
		{
			that.setNormalState();
		}
		else
		{
			that.setInvalidState();
		}
	});

	this._jRoot.find('.free_time_item').each(function()
	{
		var freeTimeItem = new FreeTimeItem(that._sb, $(this));
		that._freeTimeItems.push(freeTimeItem);

		if(freeTimeItem.currentDate.getTime() < that.currentDate.getTime())
		{
			freeTimeItem.setDisabledState();
		}
	});
}

/**
 * @private
 * @return {boolean}
 */
FreeTimeOnWeek.prototype.isValid = function()
{
	var isValid = true;
	for(var i= 0, len = this._freeTimeItems.length; i < len; i++)
	{
		isValid = isValid && this._freeTimeItems[i].isValid();
	}

	return isValid;
}

FreeTimeOnWeek.prototype.setNormalState = function()
{
	this._jRoot.removeClass('error');
	this._jError.hide();
}

FreeTimeOnWeek.prototype.setInvalidState = function()
{
	this._jRoot.addClass('error');
	this._jError.show();
}