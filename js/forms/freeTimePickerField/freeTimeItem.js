/**
 * @param {Sandbox} sb
 * @param {jQueryObject} jRoot
 * @constructor
 */
function FreeTimeItem (sb, jRoot)
{
	/**
	 * @type {FreeTimeItem}
	 */
	var that = this;

	/**
	 * @type {Sandbox}
	 * @private
	 */
	this._sb = sb;

	/**
	 * @type {jQueryObject}
	 * @private
	 */
	this._jRoot = jRoot;

	var jRootTimeStart = this._jRoot.find('.free_time_item__time_start');
	var jRootTimeEnd = this._jRoot.find('.free_time_item__time_end');
	/**
	 * @type {TimeBoundField}
	 * @private
	 */
	this._startTimeBoundField = new TimeBoundField(this._sb, jRootTimeStart, BoundType.Start);

	/**
	 * @type {TimeBoundField}
	 * @private
	 */
	this._endTimeBoundField = new TimeBoundField(this._sb, jRootTimeEnd, BoundType.End);

	/**
	 * @type {string}
	 */
	var dateStr = /** @type {string} */(this._jRoot.attr('date'));

	/**
	 * @type {Date}
	 * @public
	 */
	this.currentDate = new Date(dateStr);

	this._sb.addObserver(this.startValueChangedHandler, this);
	this._sb.addObserver(this.endValueChangedHandler, this);
}

/**
 * @private
 */
FreeTimeItem.prototype.startValueChangedHandler = function(eventName, data)
{
	if(eventName !== 'timeBoundFieldChanged')
	{
		return;
	}

	if(data.sender !== this._startTimeBoundField)
	{
		return;
	}

	var couldSet = this.couldSetDefaultValueTo(this._startTimeBoundField, this._endTimeBoundField);
	if(couldSet)
	{
		this._endTimeBoundField.setDefaultValue();
	}
}

/**
 * @private
 */
FreeTimeItem.prototype.endValueChangedHandler = function(eventName, data)
{
	if(eventName !== 'timeBoundFieldChanged')
	{
		return;
	}

	if(data.sender !== this._endTimeBoundField)
	{
		return;
	}

	var couldSet = this.couldSetDefaultValueTo(this._endTimeBoundField, this._startTimeBoundField);
	if(couldSet)
	{
		this._startTimeBoundField.setDefaultValue();
	}
}

/**
 *
 * @param {TimeBoundField} mainControl
 * @param {TimeBoundField} partnerControl
 * @return {boolean}
 */
FreeTimeItem.prototype.couldSetDefaultValueTo = function(mainControl, partnerControl)
{

	return mainControl.isValueSet() && !partnerControl.isValueSet();

}

/**
 * @private
 */
FreeTimeItem.prototype.raiseValueChangedEvent = function()
{

}

/**
 * @public
 * @return {?Date}
 */
FreeTimeItem.prototype.getStartDateTime = function()
{

}

/**
 * @public
 * @return {?Date}
 */
FreeTimeItem.prototype.getEndDateTime = function()
{

}

/**
 * @param {ControlState} state
 */
FreeTimeItem.prototype.setState = function(state)
{
	switch (state)
	{
		case ControlState.Disabled:
			this.setDisabledState();
			break;

		case ControlState.Enabled:
			this.setEnabledState();
			break;

		default :
			throw  {msg: "NotImplementedException"};
	}
}

/**
 * @public
 */
FreeTimeItem.prototype.setDisabledState = function()
{
	this._startTimeBoundField.setDisabledState();
	this._endTimeBoundField.setDisabledState();
}

/**
 * @public
 */
FreeTimeItem.prototype.setEnabledState = function()
{
	this._startTimeBoundField.setEnabledState();
	this._endTimeBoundField.setEnabledState();
}

/**
 * @public
 * @return {boolean}
 */
FreeTimeItem.prototype.isValid = function()
{
	/**
	 * @type {TimeBoundField}
	 */
	var startField = this._startTimeBoundField;
	/**
	 * @type {TimeBoundField}
	 */
	var endField = this._endTimeBoundField;

	/**
	 * Не задано ни начало, ни конец
	 * @type {boolean}
	 */
	var startAndEndHasNoValue = !startField.isValueSet() && !endField.isValueSet();
	/**
	 * Задано как начало так и конец
	 * @type {boolean}
	 */
	var startAndEndHasValue = startField.isValueSet() && endField.isValueSet();

	var isValuesCorrect = true;
	if(startAndEndHasValue)
	{
		// Сравнение происходит в алфавитном порядке '11:00' < '13:30' true
		isValuesCorrect = startField.getSelectedValue() < endField.getSelectedValue();
	}

	return startAndEndHasNoValue || (startAndEndHasValue && isValuesCorrect);


}
