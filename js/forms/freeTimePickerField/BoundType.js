/**
 * Определяет тип границы — нижняя или верхняя
 * @enum {number}
 */
var BoundType =
{
	Start : 1,
	End : 2
}
