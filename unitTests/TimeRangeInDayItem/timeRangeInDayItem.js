this.timeRangeInDayItemTest =
{
	'creation' : function(test)
	{
		var date = new Date(2013, 1, 1, 0, 0, 0, 0);
		var tr = new TimeRangeInDayItem(null, date);

		test.equal(tr.start, null, 'start property is valid');
		test.equal(tr.end, null, 'end property is valid');

		test.done();
	},

	'parse hours and minutes' : function(test)
	{
		var date = new Date(2013, 1, 1, 0, 0, 0, 0);
		var tr = new TimeRangeInDayItem(null, date);

		var arr = tr.parseHoursAndMinutes('11:30');

		test.equal(arr[0], 11, 'parse hours pass!');
		test.equal(arr[1], 30, 'parse minutes pass!');

		test.done();
	},

	'convert to date no_value string' : function(test)
	{
		var date = new Date(2013, 1, 1, 0, 0, 0, 0);
		var tr = new TimeRangeInDayItem(null, date);

		var d = tr.convertToDate('no_value');

		test.equal(d, null,'no_value parse to null');
		test.done();
	},

	'convert to date valid string' : function(test)
	{
		var date = new Date(2013, 1, 1, 0, 0, 0, 0);
		var tr = new TimeRangeInDayItem(null, date);

		var d = tr.convertToDate('11:30');

		var year = d.getFullYear();
		var month = d.getMonth();
		var day = d.getDate();
		var hour = d.getHours();
		var minutes = d.getMinutes();

		test.equal(year, 2013,'year is correct');
		test.equal(month, 1,'month is correct');
		test.equal(day, 1,'day is correct');
		test.equal(hour, 11,'hour is correct');
		test.equal(minutes, 30,'minutes is correct');

		test.done();
	},

	'convert date to string' : function(test)
	{
		var date = new Date(2013, 1, 1, 0, 0, 0, 0);
		var tr = new TimeRangeInDayItem(null, date);

		var dateToConvert = new Date(2013, 1, 1, 11, 30, 0, 0);
		var dateStr = tr.convertDateToString(dateToConvert);

		test.equal(dateStr, '11:30', 'convert date to string pass');

		test.done();
	},

	'get formatted time property with null date' : function(test)
	{
		var date = new Date(2013, 1, 1, 0, 0, 0, 0);
		var tr = new TimeRangeInDayItem(null, date);

		var result = tr.getFormattedTimeProperty(null);

		test.equal(result, 'no_value', 'convert null to time string');

		test.done();
	},

	'boundaries are not set' : function(test)
	{
		var date = new Date(2013, 1, 1, 0, 0, 0, 0);
		var sb = new Sandbox();
		var tr = new TimeRangeInDayItem(sb, date);


		tr.setTimeRange('10:00', 'no_value');
		var arg = {};
		var pass = tr.boundariesAreSet(arg);

		test.equal(pass, false, pass);
		test.notEqual(arg.Description, null, arg.Description);
		test.ok(arg.Description === tr._errorDescription);

		test.done();
	},

	'boundaries are set' : function(test)
	{
		var date = new Date(2013, 1, 1, 0, 0, 0, 0);
		var sb = new Sandbox();
		var tr = new TimeRangeInDayItem(sb, date);


		tr.setTimeRange('10:00', '11:00');
		var arg = {};
		var pass = tr.boundariesAreSet(arg);

		test.equal(pass, true, pass);
		test.equal(arg.Description, null, arg.Description);
		test.equal(tr._errorDescription, null);

		test.done();
	},

	'boundaries both are not set' : function(test)
	{
		var date = new Date(2013, 1, 1, 0, 0, 0, 0);
		var sb = new Sandbox();
		var tr = new TimeRangeInDayItem(sb, date);


		tr.setTimeRange('no_value', 'no_value');
		var arg = {};
		var pass = tr.boundariesAreSet(arg);

		test.equal(pass, true, pass);
		test.equal(arg.Description, null);
		test.equal(tr._errorDescription, null);

		test.done();
	},

	'end must be grater than start' : function(test)
	{
		var date = new Date(2013, 1, 1, 0, 0, 0, 0);
		var sb = new Sandbox();
		var tr = new TimeRangeInDayItem(sb, date);

		tr.setTimeRange('20:00', '19:30');
		var arg = {};
		var pass = tr.rightOrder(arg);

		test.equal(pass, false, pass);
		test.notEqual(arg.Description, null, arg.Description);
		test.ok(tr._errorDescription === arg.Description);

		test.done();
	},

	'right order' : function(test)
	{
		var date = new Date(2013, 1, 1, 0, 0, 0, 0);
		var sb = new Sandbox();
		var tr = new TimeRangeInDayItem(sb, date);

		tr.setTimeRange('20:00', '22:00');
		var arg = {};
		var pass = tr.rightOrder(arg);

		test.equal(pass, true, pass);
		test.ok(arg.Description === null);
		test.ok(tr._errorDescription === null);

		test.done();
	},

	'set start and end' : function(test)
	{
		test.expect(4);

		var date = new Date(2013, 1, 1, 0, 0, 0, 0);
		var sb = new Sandbox();
		sb.addObserver(function(eventName)
		{
			// Проверяем что событие вызвано
			test.ok(true, 'timeRangeChanged');
		});
		var tr = new TimeRangeInDayItem(sb, date);

		var start = new Date(2013, 1, 1, 10, 0, 0, 0);
		var end = new Date(2013, 1, 1, 20, 30, 0, 0);
		tr.setTimeRange('10:00', '20:30');

		test.equal(tr.start.getTime(), start.getTime(), 'start property is valid');
		test.equal(tr.end.getTime(), end.getTime(), 'end property is valid');
		test.equal(tr._errorDescription, null, 'no errors');

		test.done();
	}
}
